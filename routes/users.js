var express = require('express');
var router = express.Router();
var mongo = require('mongodb').MongoClient;
var objectId = require('mongodb').ObjectID;
var assert = require('assert');

/*URL To connect to MongoDB
* DBName : UserDb
* Collections : user_info
* */
var url = 'mongodb://localhost:27017/UserDb';

/*Endpoint to go to addUser page*/
router.get('/addUser', function(req, res, next) {
    res.render('addUser',{title:'Add User'});
});

router.post('/addingUser', function(req, res,next){
    var user = {
        name : req.body.username,

        contact : {
            mobileNumber : req.body.mobileNo,
            telephone : req.body.telephone,
            email : req.body.email
        },
        address : {
            line1 : req.body.line1,
            line2 : req.body.line2,
            street : req.body.street,
            city : req.body.city,
            state : req.body.state,
            country : req.body.country
        }
    };
    mongo.connect(url, function (err, db) {
        assert.equal(null , err);
        db.collection('user_info').insertOne(user , function (err, result) {
            assert.equal(null,err);
            console.log('success added');
            db.close();
        });

    });
    res.redirect('/users/viewAll');
});


/*Endpoint to go to viewAllUsers page*/
router.get('/viewAll', function (req , res , next) {
    var userArray = [];

    mongo.connect(url, function (err, db) {
        assert.equal(null, err);
        var cursor = db.collection('user_info').find();
        cursor.forEach(function (doc, err) {
            assert.equal(null,err);
            userArray.push(doc);
        },function () {
            db.close();
            // console.log(userArray);
            userArray.forEach(function (user) {
            })
            res.render('viewAllUsers' , {title : 'View All Users',userinfo : userArray});
        });

    });
});


/*Endpoint to go to updateUser page*/
router.get('/updateUser', function (req , res , next) {
    var id = req.query.id;
    var userArray = [];
    mongo.connect(url, function (err, db) {
        assert.equal(null, err);
        var user = db.collection('user_info').findOne({"_id" : new objectId(id)}).then(function(user) {
            db.close();
            res.render('updateUser',{title : 'Update User', user : user});

        });
        //console.log('user info : '+user.name);


    });
});


router.post('/updatingUser' , function (req , res) {
    var user = {
        name : req.body.username,

        contact : {
            mobileNumber : req.body.mobileNo,
            telephone : req.body.telephone,
            email : req.body.email
        },
        address : {
            line1 : req.body.line1,
            line2 : req.body.line2,
            street : req.body.street,
            city : req.body.city,
            state : req.body.state,
            country : req.body.country
        }
    };
    var id = req.body.id;
    mongo.connect(url, function (err, db) {
        assert.equal(null, err);
        db.collection('user_info').updateOne({"_id" : objectId(id)} , {$set:user} , function (err , result) {
            assert.equal(null , err);
            console.log('User Updated');
        })

    });
    res.redirect('/users/viewAll');
});

router.get('/deleteUser' , function (req , res) {
    var id = req.query.id;
    mongo.connect(url, function (err, db) {
        assert.equal(null, err);
        db.collection('user_info').deleteOne({"_id" : objectId(id)} , function (err , result) {
            assert.equal(null , err);
            console.log('User Deleted');
        })

    });
    res.redirect('/users/viewAll');
});

module.exports = router;
